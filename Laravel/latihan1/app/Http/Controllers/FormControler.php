<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormControler extends Controller
{
    public function regist(){
        return view('halaman.regist');
    }

    public function send(Request $request){
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $gender = $request['gender'];
        $nasional = $request['nasional'];
        $language = $request['language'];
        $txtname = $request['txtname'];

        return view('halaman.wellcome', compact('firstname', 'lastname', 'gender', 'nasional', 'language', 'txtname'));
    }
}
