<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Welcome</title>
</head>
<body>
    <h1>Selamat Datang Di Sanbercode</h1>
    <h2>Terimakasih sudah bergabung di Sanbercode. Sosial Media kita Bersama</h2>
    <hr>

    <p>Nama Depan : {{ $firstname }} </p>
    <p>Nama Belakang : {{ $lastname }} </p>
    <p>Jenis Kelamin : {{ $gender }} </p>
    <p>Negara : {{ $nasional }} </p>
    <p>Bahasa : {{ $language }} </p>
    <p>Komentar : {{ $txtname }} </p>
    

    <hr>
</body>
</html> 