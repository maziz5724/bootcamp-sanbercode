@extends('layout.master')

@section('title')

    Detail Kategori {{ $cast -> nama }}

@endsection

@section('content')

    <h1>{{ $cast -> nama }}</h1>
    <p>{{ $cast -> bio }}</p>

    <a href="/cast" class="btn btn-success">Kembali</a>
@endsection