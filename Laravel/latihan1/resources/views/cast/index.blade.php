@extends('layout.master')

@section('title')

    List Cast 

@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary mb-3">Tambah Data Cast</a>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item -> nama }}</td>
                    <td>{{ $item -> umur }}</td>
                    <td>{{ $item -> bio }}</td>
                    <td>
                        
                        <form action="/cast/{{ $item -> id }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{ $item -> id }}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{ $item -> id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                    </td>
                </tr>

            @empty
                <tr>
                    <td>Data Masih Kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>

@endsection