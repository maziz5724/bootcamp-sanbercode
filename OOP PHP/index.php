<?php 
require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$domba = new Animal("Shaun");

$domba -> set_legs (4);
$domba -> set_coldBloded("No");

echo "Nama : ". $domba -> get_name(). "<br>";
echo "Legs : ". $domba -> get_legs(). "<br>";
echo "Cold Blooded : ". $domba -> get_coldBloded(). "<br> <br>";


$kodok = new Frog("Buduk");

$kodok -> set_legs (4);
$kodok -> set_coldBloded("No");

echo "Nama : ". $kodok -> get_name(). "<br>";
echo "Legs : ". $kodok -> get_legs(). "<br>";
echo "Cold Blooded : ". $kodok -> get_coldBloded(). "<br>";
echo $kodok -> Jump(). "<br> <br>";


$sungokong = new Ape("Kera Sakti");

$sungokong -> set_legs (2);
$sungokong -> set_coldBloded("No");

echo "Nama : ". $sungokong -> get_name(). "<br>";
echo "Legs : ". $sungokong -> get_legs(). "<br>";
echo "Cold Blooded : ". $sungokong -> get_coldBloded(). "<br>";
echo $sungokong -> yel();





?>